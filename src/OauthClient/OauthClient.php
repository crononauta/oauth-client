<?php

namespace OauthClient;

use GuzzleHttp\Client as GuzzleClient;

/**
 * OauthClient
 *
 * @package Crononauta
 * @version $Id$
 */
class OauthClient {

    /**
     *
     */
    const TOKEN_BASE_64  = "TOKEN_BASE_64";
    const TOKEN_PLAIN    = "TOKEN";

    /**
     * username
     *
     * @var mixed
     */
    protected $username;
    protected $password;
    protected $urlApiParams;
    protected $urlApiBase;
    protected $client;

    /**
     * __construct
     *
     * @param mixed $username
     * @param mixed $password
     * @param mixed $client
     * @param mixed $urlApiBase
     * @param mixed $urlApiParams
     * @return void
     */
    public function __construct($username,$password,$client,$urlApiBase,$urlApiParams) {
      $this->username     = $username;
      $this->password     = $password;
      $this->urlApiParams = $urlApiParams;
      $this->urlApiBase   = $urlApiBase;
      $this->client       = $client;
    }

    /**
     * getToken
     *
     * @param mixed $modeReturn
     * @return void
     */
    public function getToken($modeReturn=self::TOKEN_PLAIN) {

      $token = null;

      if(empty($this->username) || empty($this->password) || empty($this->client) || empty($this->urlApiParams) || empty($this->urlApiBase)) {
        throw new \InvalidArgumentException('Need to pass all parameters.');
      }

      $options["body"]["username"] = $this->username;
      $options["body"]["password"] = $this->password;
      $options["body"]["grant_type"] = "password";
      $options["body"]["client_id"] = $this->client;

      $client   = new GuzzleClient(array("base_uri"=> $this->urlApiBase));
      try {
        $response = $client->post($this->urlApiParams, [
          'headers' => ['Content-Type' => 'application/json'],
          'body'    => json_encode($options["body"]), 
        ]);
      } catch( \Exception $e){
        throw new \Exception("Error in post to connect a Oauth : ".$e->getMessage());
      }

      if ($response->getStatusCode() != 200) {
        throw new \Exception("Error status code in request to OauthAPi ".$response->getStatusCode());
      }else{
        $content = $response->getBody();

        try {
          $content = json_decode($content, true);
          $token = $content["access_token"];

          switch($modeReturn) {
            case self::TOKEN_BASE_64:
              $token = base64_encode($token);
              break;
          }

        } catch(\Exception $e) {
          throw new \Expcetion("Error decoding content json in request to OauthAPi ". $response->getStatusCode());
        }

      }

      return $token;

    }

}
